package sample;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
class HighScore {
    private String nickname;
    private int score;
}
