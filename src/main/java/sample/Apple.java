package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Random;

class Apple extends GameObject {

    private Random random;
    private Snake snake;

    Apple(Canvas canvas, Snake snake) {
        this.snake = snake;
        this.canvas = canvas;
        random = new Random();
        color = Color.RED;
        size = 20;
        respawn();
    }

    void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(color);
        gc.fillOval(x, y, size, size);
    }

    void respawn() {
        x = random.nextInt((int) canvas.getWidth() / 20) * 20;
        y = random.nextInt((int) canvas.getHeight() / 20) * 20;

        for (Trail trail : snake.getTrails()) {
            if (x == trail.getX() && y == trail.getY()) {
                respawn();
            }
        }
    }
}
