package sample;

import javafx.scene.control.Label;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

class HighScoreHelper {

    private List<HighScore> highScores;
    private Label highScoresLabel;

    HighScoreHelper(Label highScoresLabel) {
        this.highScoresLabel = highScoresLabel;
    }

    void addScore(String nickname, int score) {
        loadScore();
        highScores.add(new HighScore(nickname, score));
        highScores.sort(Comparator.comparingInt(HighScore::getScore));
        Collections.reverse(highScores);
        highScores = highScores.stream().limit(5).collect(Collectors.toList());

        try {
            PrintWriter out = new PrintWriter("high-score.txt");
            highScores.forEach(x -> out.println(x.getNickname() + ":" + x.getScore()));
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    boolean isCurrentScoreNewRecord(int score) {
        loadScore();
        return highScores.size() < 5 || highScores.get(highScores.size() - 1).getScore() < score;
    }

    void displayHighScore() {
        loadScore();
        highScores.sort(Comparator.comparingInt(HighScore::getScore));
        Collections.reverse(highScores);

        StringBuilder output = new StringBuilder();

        highScores.forEach(x -> output.append(x.getNickname()).append(" : ").append(x.getScore()).append("\n"));

        highScoresLabel.setText(output.toString());
    }

    private void loadScore() {
        highScores = new ArrayList<>();
        try {
            Scanner file = new Scanner(new File("high-score.txt"));
            while (file.hasNextLine()) {
                String[] line = file.nextLine().split(":");
                if (line.length == 2) {
                    highScores.add(new HighScore(line[0], Integer.parseInt(line[1])));
                }
            }
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
