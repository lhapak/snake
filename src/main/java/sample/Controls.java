package sample;

import javafx.scene.input.KeyEvent;

class Controls {

    private Snake snake;

    Controls(Snake snake) {
        this.snake = snake;
    }

    void onKeyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case UP:
            case W:
                if (snake.getYVelocity() <= 0) {
                    snake.setXVelocity(0);
                    snake.setYVelocity(-snake.getSpeed());
                }
                break;
            case DOWN:
            case S:
                if (snake.getYVelocity() >= 0) {
                    snake.setXVelocity(0);
                    snake.setYVelocity(snake.getSpeed());
                }
                break;
            case LEFT:
            case A:
                if (snake.getXVelocity() <= 0) {
                    snake.setYVelocity(0);
                    snake.setXVelocity(-snake.getSpeed());
                }
                break;
            case RIGHT:
            case D:
                if (snake.getXVelocity() >= 0) {
                    snake.setYVelocity(0);
                    snake.setXVelocity(snake.getSpeed());
                }
                break;
        }
    }
}
