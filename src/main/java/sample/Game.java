package sample;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

public class Game {
    public Canvas canvas;
    public Pane pane;
    public Pane menuView;
    public Pane gameView;
    public Pane newRecordView;
    public Pane highScoreView;
    public Label currentScoreLabel;
    public TextField nicknameTextField;
    public Label highScoresLabel;

    private Snake snake;
    private Background background;
    private Apple apple;
    private Controls controls;
    private HighScoreHelper highScoreHelper;
    private int time;
    private int score;
    private AnimationTimer animationTimer;

    public void initialize() {
        background = new Background(canvas);
        snake = new Snake(canvas);
        apple = new Apple(canvas, snake);
        snake.setApple(apple);
        controls = new Controls(snake);
        highScoreHelper = new HighScoreHelper(highScoresLabel);

        pane.setOnKeyPressed(event -> controls.onKeyPressed(event));

        animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
            }
        };
    }

    public void startGame() {
        menuView.setVisible(false);
        gameView.setVisible(true);
        animationTimer.start();
        time = 0;
        score = 0;
        apple.respawn();
        snake.restart();
    }

    public void openHighScore() {
        menuView.setVisible(false);
        highScoreHelper.displayHighScore();
        highScoreView.setVisible(true);
    }

    public void addNewRecord() {
        highScoreHelper.addScore(nicknameTextField.getText(), score);
        newRecordView.setVisible(false);
        menuView.setVisible(true);
    }

    public void closeHighScore() {
        highScoreView.setVisible(false);
        menuView.setVisible(true);
    }

    private void drawGameObjects() {
        background.draw();
        apple.draw();
        snake.draw();
    }

    private void update() {
        time++;
        if (time == 6) {
            time = 0;
            snake.move();

            if (snake.isCollidingApple()) {
                apple.respawn();
                score++;
                snake.setLength(snake.getLength() + 1);
            }

            if (snake.isCollidingTrail()) {
                endGame();
            }

            drawGameObjects();

            currentScoreLabel.setText("Score: " + score);
        }
    }

    private void endGame() {
        animationTimer.stop();
        gameView.setVisible(false);
        if (highScoreHelper.isCurrentScoreNewRecord(score)) {
            newRecordView.setVisible(true);
        } else {
            menuView.setVisible(true);
        }
    }
}
