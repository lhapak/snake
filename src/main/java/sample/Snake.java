package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
class Snake extends GameObject {

    private int xVelocity;
    private int yVelocity;
    private int speed;
    private Apple apple;
    private List<Trail> trails;
    private int length;

    Snake(Canvas canvas) {
        this.canvas = canvas;
        size = 20;
        speed = 20;
        color = Color.YELLOW;
        trails = new ArrayList<>();
        restart();
    }

    void draw() {
        generateTrail();
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(color);
        gc.fillRect(x, y, size, size);
        drawTrail();
    }

    void move() {
        if (yVelocity > 0) {
            if (y < canvas.getHeight() - size) {
                y += yVelocity;
            }
        } else if (yVelocity < 0) {
            if (y > 0) {
                y += yVelocity;
            }
        }
        if (xVelocity > 0) {
            if (x < canvas.getWidth() - size) {
                x += xVelocity;
            }
        } else if (xVelocity < 0) {
            if (x > 0) {
                x += xVelocity;
            }
        }
    }

    boolean isCollidingApple() {
        return x == apple.getX() && y == apple.getY();
    }

    boolean isCollidingTrail() {
        for (Trail trail : trails) {
            if (x == trail.getX() && y == trail.getY()) {
                return true;
            }
        }
        return false;
    }

    void restart() {
        x = 100;
        y = 100;
        length = 3;
        trails = new ArrayList<>();
        xVelocity = speed;
        yVelocity = 0;
    }

    private void generateTrail() {
        trails.add(new Trail(x, y));

        while (trails.size() > length) {
            trails.remove(0);
        }
    }

    private void drawTrail() {
        for (Trail trail : trails) {
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.fillRect(trail.getX() + 1, trail.getY() + 1, trail.getSize() - 2, trail.getSize() - 2);
        }
    }
}
