package sample;

import javafx.scene.paint.Color;
import lombok.Getter;

@Getter
class Trail {

    private int size;
    private Color color;
    private int x;
    private int y;

    Trail(int x, int y) {
        this.x = x;
        this.y = y;
        size = 20;
        color = Color.YELLOW;
    }
}