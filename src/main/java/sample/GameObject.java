package sample;

import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
abstract class GameObject {

    Color color;
    Canvas canvas;
    int size;
    int x;
    int y;

    abstract void draw();
}
